from django.test import TestCase
from ..models import *


class LogTesteCase(TestCase):
    def teste_retorna_dados_log(self):
        log = LogCentral.objects.create(
            id=1,
            descricao='user.Service.Auth: password.Password',
            endereco_origem='127.0.0.1',
            nivel='warning',
            qtdade_eventos=300,
            ambiente='Dev',
            detalhe='user.Service.Auth: password.Password.Compare: crypto/bcrypt (...)',
            arquivar=False,
            data_hora='2019-05-24T09:53:10.001Z',
        )

        self.assertEqual(str(log), 'user.Service.Auth: password.Password')
