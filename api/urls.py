from django.urls import path, include, re_path
from rest_framework import routers

from rest_framework.authtoken.views import obtain_auth_token
from api import views


router = routers.DefaultRouter()
router.register('logs', views.LogViewset)


urlpatterns = [
    path('', include(router.urls)),
    re_path(r'cadastro_usuario/', views.UserAPIView.as_view()),
    path('login_usuario/', obtain_auth_token),
    re_path(r'log_id/(?P<pk>\d+)', views.LogPorIdAPIView.as_view()),
    re_path(r'cadastro_log/', views.LogAPIView.as_view()),
]
