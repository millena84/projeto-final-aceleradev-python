from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.permissions import IsAuthenticated
from rest_framework.authentication import TokenAuthentication
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status, viewsets
from rest_framework import filters
from rest_framework.filters import OrderingFilter


from .serializers import *

# Create your views here.


class UserAPIView(APIView):
    def post(self, request):
        username = request.data.get("username")
        password = request.data.get("password")
        user, created = User.objects.get_or_create(username=username)
        user.set_password(password)
        try:
            user.save()
            return Response({"mensagem": "Usuário cadastrado"}, status=status.HTTP_201_CREATED)
        except:
            return Response(status=status.HTTP_400_BAD_REQUEST)


class LogAPIView(APIView):

    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def post(self, request):
        serializer = LogCentralModelSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class LogPorIdAPIView(APIView):

    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get(self, request, pk):
        try:
            queryset = LogCentral.objects.get(pk=pk)
            serializer_class = LogCentralModelSerializer(queryset, many=False)
            if serializer_class:
                return Response(serializer_class.data, status=status.HTTP_200_OK)
        except:
            return Response({
                                "mensagem": "Não foi encontrado log, com este id, cadastrado"
                            },
                            status=status.HTTP_404_NOT_FOUND)

    def put(self, request, pk):
        try:
            queryset = LogCentral.objects.get(pk=pk)
            serializer = LogCentralModelSerializer(data=request.data, instance=queryset)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_200_OK)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        except:
            return Response({
                                "mensagem": "Não foi encontrado log, com este id, cadastrado"
                            },
                            status=status.HTTP_404_NOT_FOUND)

    def delete(self, request, pk):
        try:
            queryset = LogCentral.objects.get(pk=pk)
            if queryset is not None:
                queryset.delete()
                return Response({
                                    "mensagem": "Registro deletado"
                                }, status=status.HTTP_200_OK)
            return Response(status=status.HTTP_400_BAD_REQUEST)
        except:
            return Response({
                                "mensagem": "Não foi encontrado log, com este id, cadastrado"
                            },
                            status=status.HTTP_404_NOT_FOUND)


class LogViewset(viewsets.ModelViewSet):

    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    queryset = LogCentral.objects.all()
    serializer_class = LogCentralModelSerializer

    filter_backends = [DjangoFilterBackend,
                       OrderingFilter,
                       filters.SearchFilter,]

    filterset_fields = ['ambiente']
    ordering_fields = ['nivel', 'qtdade_eventos']
    search_fields = ['nivel', 'endereco_origem', 'descricao']