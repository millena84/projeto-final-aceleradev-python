from rest_framework import serializers
from django.contrib.auth.models import User
from .models import *


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        #extra_kwargs = {'password': {'write_only': True}}
        fields = ('username', 'password')


class LogCentralModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = LogCentral
        fields = ['id',
                  'descricao',
                  'endereco_origem',
                  'nivel',
                  'qtdade_eventos',
                  'ambiente',
                  'detalhe',
                  'arquivar',
                  'data_hora']