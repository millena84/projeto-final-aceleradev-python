from django.core.validators import validate_email, \
                                    MinLengthValidator, \
                                    validate_ipv4_address
from django.db import models


# Create your models here.


class LogCentral(models.Model):
    NIVEL_CHOICES = [
        ('Critical', 'Critical'),
        ('Debug', 'Debug'),
        ('Error', 'Error'),
        ('Warning', 'Warning'),
        ('Information', 'Information')
    ]

    AMBIENTE_CHOICES = [
        ('Produção', 'Produção'),
        ('Homologação', 'Homologação'),
        ('Dev', 'Dev')
    ]

    descricao = models.CharField('Descrição', max_length=50)
    endereco_origem = models.GenericIPAddressField('Endereço',
                                                   max_length=39,
                                                   protocol='both',
                                                   validators=[validate_ipv4_address])
    nivel = models.CharField('Nível', choices=NIVEL_CHOICES, max_length=20)
    qtdade_eventos = models.IntegerField('Quantidade de eventos', default=0)
    ambiente = models.CharField('Ambiente', choices=AMBIENTE_CHOICES, max_length=20)
    detalhe = models.TextField('Detalhe')
    arquivar = models.BooleanField('Arquivar', default=False)
    data_hora = models.DateTimeField('Data/Hora', auto_now_add=True)

    def __str__(self):
        return self.descricao
